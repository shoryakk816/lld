package com.example.lld.eventcalendar.db;

import com.example.lld.eventcalendar.datamodels.Event;
import com.example.lld.eventcalendar.utils.ArrayUtils;
import lombok.Getter;
import lombok.Synchronized;

import java.sql.Time;
import java.util.*;

@Getter
public class EventDb {

    private Map<String, Set<String>> eventParticpantsMap;

    private Map<String, String> userEventMap;

    private Map<String, Event> eventIdEventMap;

    private static EventDb instance;

    @Synchronized
    public static EventDb getInstance() {
        if (instance == null) {
            instance = new EventDb();
        }

        return instance;
    }

    private EventDb() {
        this.eventParticpantsMap = new HashMap<>();
        this.eventIdEventMap = new HashMap<>();
        this.userEventMap = new HashMap<>();
    }

    public String registerEvent(Time start, Time end, Set<String> teams, Set<String> participants) {
        Set<String> users = new HashSet<>();
        if (ArrayUtils.isValid(teams)) {
            teams.forEach(teamId -> users.addAll(UserDb.getInstance().getTeamUserMap().get(teamId)));
        }
        participants.addAll(users);

        String eventId = UUID.randomUUID().toString();
        Event event = new Event();
        event.setId(eventId);
        event.setName("Random: " + eventId);
        event.setUsers(participants);
        event.setStartDate(new Date());
        event.setEndDate(new Date());
        event.setStartTime(start);
        event.setEndTime(end);

        eventParticpantsMap.put(eventId, participants);
        participants.forEach(id -> userEventMap.put(id, eventId));
        eventIdEventMap.put(eventId, event);

        return eventId;
    }

    public void deleteEvent(String eventId) {
        Set<String> participants = eventParticpantsMap.get(eventId);
        participants.forEach(id -> userEventMap.remove(id));
        eventIdEventMap.remove(eventId);
        eventParticpantsMap.remove(eventId);
    }

    public Event getEventForUser(String userId) {
        return eventIdEventMap.get(userEventMap.get(userId));
    }
}
