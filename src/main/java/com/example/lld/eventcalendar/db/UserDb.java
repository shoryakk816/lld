package com.example.lld.eventcalendar.db;

import com.example.lld.eventcalendar.datamodels.Team;
import com.example.lld.eventcalendar.datamodels.User;
import com.example.lld.eventcalendar.utils.ArrayUtils;
import com.example.lld.eventcalendar.utils.StringUtils;
import lombok.Getter;
import lombok.Synchronized;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class UserDb {

    private Map<String, User> idUserMap;

    private Map<String, String> userTeamMap;

    private Map<String, Set<String>> teamUserMap;

    private static UserDb instance;

    @Synchronized
    public static UserDb getInstance() {
        if (instance == null) {
            instance = new UserDb();
        }
        return instance;
    }

    private UserDb() {
        this.idUserMap = new HashMap<>();
        this.teamUserMap = new HashMap<>();
        this.userTeamMap = new HashMap<>();
    }

    // Think about working hours logic
    public User registerUser(String contact, String name) {
        if (StringUtils.isInvalid(contact, name)) {
            return null;
        }

        String userId = StringUtils.createUserId(contact);
        User user = new User(userId, name, contact);
        idUserMap.put(userId, user);

        return user;
    }

    public String registerTeam(Set<String> users) {
        String teamId = UUID.randomUUID().toString();

        users.forEach(userId -> {
            userTeamMap.put(userId, teamId);
        });

        teamUserMap.put(teamId, users);

        return teamId;
    }

    public User findUser(String contact) {
        return idUserMap.get(StringUtils.createUserId(contact));
    }

    public void associateUserToTeam(String userId, String teamId) {
        userTeamMap.put(userId, teamId);

        Set<String> users = teamUserMap.getOrDefault(teamId, new HashSet<>());
        users.add(userId);
        teamUserMap.put(teamId, users);
    }

    public List<User> getUserFromTeam(String teamId) {
        Set<String> users = teamUserMap.get(teamId);
        if (!ArrayUtils.isValid(users)) {
            return null;
        }

        return users.stream()
                .map(userId -> idUserMap.get(userId))
                .collect(Collectors.toList());
    }

    public Team fetchTeam(String teamId) {
        if (StringUtils.isInvalid(teamId)) return null;

        return new Team(teamId, getUserFromTeam(teamId));
    }

    public Team fetchTeamFromUserId(String contact) {
        String userId = StringUtils.createUserId(contact);
        String teamId = userTeamMap.get(userId);
        return new Team(teamId, getUserFromTeam(teamId));
    }

    public void removeTeam(String teamId) {
        Set<String> users = teamUserMap.get(teamId);
        users.forEach(userId -> userTeamMap.remove(userId));
        teamUserMap.remove(teamId);
    }

    public void removeUser(String userId) {
        String teamId = userTeamMap.get(userId);
        teamUserMap.get(teamId).remove(userId);
        userTeamMap.remove(userId);
        idUserMap.remove(userId);
    }
}
