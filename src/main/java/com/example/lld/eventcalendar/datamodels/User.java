package com.example.lld.eventcalendar.datamodels;

import com.example.lld.eventcalendar.utils.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String id;

    private String name;

    // numeric only
    private String contact;

    @Override
    public String toString() {
        return JsonUtils.gson().toJson(this);
    }
}
