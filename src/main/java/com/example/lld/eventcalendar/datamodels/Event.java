package com.example.lld.eventcalendar.datamodels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    private Date startDate;

    private Date endDate;

    private Time startTime;

    private Time endTime;

    private String id;

    private String name;

    private Set<String> users;
}
