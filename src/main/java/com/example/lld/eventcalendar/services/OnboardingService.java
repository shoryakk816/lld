package com.example.lld.eventcalendar.services;


import com.example.lld.eventcalendar.datamodels.User;
import com.example.lld.eventcalendar.db.UserDb;
import com.example.lld.eventcalendar.utils.StringUtils;

public class OnboardingService {

    public static User onboardUser(String name, String contact) {
        String userId = StringUtils.createUserId(contact);

        if (UserDb.getInstance().getIdUserMap().containsKey(userId)) {

            User user = UserDb.getInstance().getIdUserMap().get(userId);
            System.out.println("User already exists: " + user);

            return user;
        }

        return UserDb.getInstance().registerUser(contact, name);
    }
}
