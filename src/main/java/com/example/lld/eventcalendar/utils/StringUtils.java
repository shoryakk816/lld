package com.example.lld.eventcalendar.utils;

public class StringUtils {

    public static boolean isValid(String st) {
        return st != null
                && !st.isEmpty()
                && !st.equalsIgnoreCase("none");
    }

    public static boolean allValid(String... strs) {
        return strs != null;
    }

    public static boolean isInvalid(String... st) {
        return !allValid(st);
    }

    public static boolean isInvalid(String st) {
        return !isValid(st);
    }

    public static String createUserId(String contact) {
        return String.valueOf(contact.hashCode());
    }
}
