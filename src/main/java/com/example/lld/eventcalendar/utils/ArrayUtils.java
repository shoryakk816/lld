package com.example.lld.eventcalendar.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ArrayUtils {

    public static <T> boolean isValid(Set<T> list) {
        return list != null
                && !list.isEmpty();
    }

    public static <T> boolean isValid(List<String> list) {
        return list != null
                && !list.isEmpty();
    }

    public static <T> boolean isValid(ArrayList<String> list) {
        return list != null
                && !list.isEmpty();
    }
}
